#!/usr/bin/env bash

export PATH=$PATH:/usr/local/bin

RC=0
for dir in $(echo "$@" | xargs -n1 dirname | sort -u | uniq); do
  dir=${dir/%\/policy/}
  if [[ -d "${dir}/policy" ]]; then
    echo "Validating \"${dir}\" folder"
    (cd "${dir}" && conftest test --combine ./*.tf)
    RC=$((RC+$?))
  else
    echo "Missing policy for \"${dir}\" folder, no validation"
  fi
done
exit ${RC}