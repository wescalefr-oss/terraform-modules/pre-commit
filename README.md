# pre-commit for Terraform

Some [pre-commit](https://github.com/pre-commit/pre-commit) hooks to execute on Terraform code:    
- terraform-conftest: for each `.tf`and `.rego` file found in list, executes a test run of [conftest](https://github.com/instrumenta/conftest) on the combined Terraform files (with ./policy content by default).

## General Usage
Add in the file called `.pre-commit-config.yaml` in your repositories with the following declaration:
```
  - repo: https://gitlab.com/wescalefr-oss/terraform-modules/pre-commit
    rev: <VERSION> # Get the latest from: https://gitlab.com/wescalefr-oss/terraform-modules/pre-commit/-/releases-/releases
    hooks:
      - id: terraform-conftest
```
